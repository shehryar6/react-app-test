import React, {useEffect} from "react";
import './create-note.scss'

const CreateNote = (props) => {
    const [noteText, setNoteText] = React.useState("");
    const [noteColor, setNoteColor] = React.useState("red");

    useEffect(() => {
        setNoteText(props.note.text)
        setNoteColor(props.note.color)

    }, [props.note])

    const changeNoteText = event => {
        setNoteText(event.target.value);
    };

    const changeNoteColor = (event) => {
        setNoteColor(event.target.value);
    };

    const addNote = () => {
        let obj = {}
        obj.color = noteColor ? noteColor : "red"
        obj.text = noteText
        obj.time = new Date()
        props.onCreate(obj);
        setNoteColor("red");
        setNoteText("")
    }

    return (
        <div className='create-form'>
            <div className='form-input'>
                <div className='input-text'>Note Text:</div>
                <input
                    className='input --full-width --buttom-shadow-light'
                    type='text'
                    placeholder='Add Note Text her...'
                    onChange={changeNoteText}
                    value={noteText}
                />
            </div>
            <div className='form-input'>
                <div className='input-text'>Note Color:</div>
                <select
                    className='input --full-width --buttom-shadow-light'
                    value={noteColor}
                    onChange={changeNoteColor}>
                    <option value="red">Red</option>
                    <option value="orange">Orange</option>
                    <option value="green">Green</option>
                    <option value="purple">Purple</option>
                    <option value="blue">Blue</option>
                    <option value="light-blue">Light Blue</option>
                </select>
            </div>
            <div className='form-input'>
                <button className='button --buttom-shadow-medium' onClick={addNote}>
                    Add Note
                </button>
            </div>
        </div>
    );
};

CreateNote.propTypes = {};

export default CreateNote;

