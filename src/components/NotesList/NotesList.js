import React from 'react';
import './notes-list.scss';
import Note from "../Note/Note";

const NotesList = (props) => {
    const {list, deleteNote} = props;

    console.log((list));

    return (
        <div className={"notes-list"}>
            {list && list.map((note, index) =>
                <Note editNote={(note) => props.editNote(note)} deleteNote={(id) => props.deleteNote(id)} key={index} note={note}/>
            )}
        </div>
    )
}

export  default NotesList;