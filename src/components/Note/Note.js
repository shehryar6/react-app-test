import React from 'react';
import './note.scss';
import EditIcon from '../../assets/edit-solid.svg';
import DeleteIcon from '../../assets/trash-solid.svg';
import {format} from 'date-fns';

const Note = (props) => {
    const {note, deleteNote} = props
    const dateFormat = "MMM d HH:MM";

    return (
        <div className={"note"}>
            <div className={"color " + note.color}> </div>

            <div className={"body"}>
                <div className={"head"}>
                    <h4 className={note.color}>{note.title}</h4>
                    <div className={"actions"}>
                        <img onClick={() => props.editNote(note)} src={EditIcon} className={"icon-ed"} alt={"test"}/>
                        <img onClick={() => props.deleteNote(note.id)} src={DeleteIcon} className={"icon-del"} alt={"test"}/>
                    </div>
                </div>
                <div className={"text"}>
                    <p>{note.text}</p>
                </div>
                <div className='foot'>
                    <p>{format(note.time, dateFormat)}</p>
                </div>
            </div>
        </div>
    )
}

export  default Note;