import React, {useState} from 'react';

import './App.scss';

import CreateNote from './components/CreateNote/CreateNote'
import NotesList from "./components/NotesList/NotesList";

function App() {
    const [list, setList] = useState([]);
    const [note, setNote] = useState({})

    const createNote = (note) => {
        let ll = [...list];
        note.title = "note " + ll.length;
        note.id = ll.length;
        ll.unshift(note);
        setList(ll)
    };

    const deleteNote = (id) =>{
        setList(list.filter((item) => item.id !== id));
    };

    const editNote = (note) =>{
        setNote(note)
    };


    return (
        <div className="App">
            <CreateNote onCreate={createNote} note={note}/>
            <div className={"list"}>
                <NotesList deleteNote={deleteNote} editNote={editNote} list={list}/>
            </div>
        </div>
    );
}

export default App;
